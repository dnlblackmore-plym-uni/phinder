docker ?= docker
docker_container_name ?= phinder_app
docker_image_name ?= phinder_ruby:3.1.2-bullseye
docker_external_port ?= 3000
docker_internal_port ?= 3000
docker_run_flags := --rm -it --name $(docker_container_name) -v ${PWD}/:/usr/src/app/:rw -p $(docker_external_port):$(docker_internal_port) $(docker_image_name)
docker_run_test_flags := --rm -it --name $(docker_container_name)_test -v ${PWD}/:/usr/src/app/:rw $(docker_image_name)
docker_run_generate_flags := --rm -it --name $(docker_container_name)_generate -v ${PWD}/:/usr/src/app/:rw $(docker_image_name)
docker_run_misc_flags := --rm -it --name $(docker_container_name)_misc -v ${PWD}/:/usr/src/app/:rw $(docker_image_name)
docker_container_flags := rails server -b 0.0.0.0 -p $(docker_internal_port)

base_docker_build_flags =
podman_docker_build_flag_additions = --squash-all

ifeq ($(docker),podman)
	docker_build_flags = $(base_docker_build_flags) $(podman_docker_build_flag_additions)
else
	docker_build_flags = $(base_docker_build_flags)
endif

g_name ?=

command ?= bash

.PHONY: development production test feature_test docker_image

development:
	$(docker) run $(docker_run_flags) $(docker_container_flags) -e development

production:
	$(docker) run $(docker_run_flags) $(docker_container_flags) -e production

test:
	$(docker) run $(docker_run_test_flags) rspec

feature_test:
	$(docker) run $(docker_run_generate_flags) rails generate rspec:feature $(g_name)

docker_container:
	$(docker) run $(docker_run_misc_flags) $(command)

docker_image: Dockerfile
	$(docker) build -t $(docker_image_name) $(docker_build_flags) -f $< .
