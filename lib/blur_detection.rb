require 'rmagick'
include Magick

# From: https://rubytalk.org/t/q-how-to-unflatten-a-flat-array/20457
# David_A_Black3
# Modifications made. Removed block support.
# Add slicing to array for rate_focus.
class Array
  def in_slices_of(n)
    res = []
    0.step(size - 1, n) do |i|
      s = slice(i...i + n)
      res.push s
    end
    res
  end
end

class BlurDetection
  # Produces 'edge_map'.
  def self.edge_detect(source_image_path)
    # Store 'source_image' as opened 'source_image_path'
    source_image = ImageList.new(source_image_path).combine
    # Return grey scale of Sobel operator of 'source_image'
    edge_map = source_image.edge(1.0)
    return edge_map
  end

  # Produces 'focus_rating'.
  def self.rate_focus(edge_map, threshold_percentage = 0.85, focus_scale = 0.75)
    dispatched_edge_map = edge_map.dispatch(0, 0, edge_map.columns, edge_map.rows, "I", true)
    # Store 'sharpest_edge' as maximum of 'edge_map'
    sharpest_edge = dispatched_edge_map.max
    # Store 'threshold' as 'sharpest_edge' * 'threshold_percentage'
    threshold = sharpest_edge * threshold_percentage
    # Store 'pass_map' as two dimensional Boolean array of ('edge_map[pixel]' >= 'threshold')
    pass_map = dispatched_edge_map.in_slices_of(edge_map.columns)
    pass_map.map! do |row|
      row.map do |pixel|
        pixel >= threshold
      end
    end
    # Store 'fail_scale' as the scale of the largest rectangle in the centre of the image that encompasses only false on the 'pass_map'
    fail_scale = 1.0
    pass_map.each_with_index do |row, y|
      half_rows = edge_map.rows / 2
      y_max = ((y - half_rows).to_f / half_rows.to_f).abs
      if y > half_rows then
        # Optimisation: Stop when fail_scale cannot be reduced.
        break if fail_scale < y_max
      end
      row.each_with_index do |pixel, x|
        half_columns = edge_map.columns / 2
        x_max = ((x - half_columns).to_f / half_columns.to_f).abs
        if x > half_columns then
          # Optimisation: Stop when fail_scale cannot be reduced.
          break if fail_scale < x_max
        end
        point_value = [x_max, y_max].max
        fail_scale = [point_value, fail_scale].min if pixel
      end
    end
    # Clamp the 'fail_scale' to the outside of 'focus_scale' and return the inverted result.
    return 1.0 - [[(fail_scale / focus_scale) - (1.0 - focus_scale), 0.0].max, 1.0].min
  end

  # Detects blur in the focused region. Produces 'image_rating'.
  def self.rate_image(edge_map, focus_rating = 1.0)
    # Store 'sharpest_edge' as maximum of 'edge_map'
    sharpest_edge = edge_map.dispatch(0, 0, edge_map.columns, edge_map.rows, "I", true).max
    return sharpest_edge * focus_rating;
  end
end
