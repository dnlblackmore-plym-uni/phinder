class PhotoTagAssociations < ActiveRecord::Migration[7.1]
  def change
    create_table :photo_tag_associations do |t|
      t.integer :photo_id
      t.integer :tag_id
    end
  end
end
