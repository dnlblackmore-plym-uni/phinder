class FocusTestImages < ActiveRecord::Migration[7.1]
  def change
    create_table :focus_test_images do |t|
      t.string :image_path
    end
  end
end
