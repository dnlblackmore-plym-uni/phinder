class CreatePhotos < ActiveRecord::Migration[7.1]
  def change
    create_table :photos do |t|
      t.string :name
      t.datetime :date
      t.float :blur_detection_index
      t.string :location

      t.timestamps
    end
  end
end
