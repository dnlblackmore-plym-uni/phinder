class AddAssociationIds < ActiveRecord::Migration[7.1]
  def change
    add_column :collections, :collection_id, :integer
    add_column :photos, :collection_id, :integer
  end
end
