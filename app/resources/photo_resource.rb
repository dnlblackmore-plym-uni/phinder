class PhotoResource < JSONAPI::Resource
  attributes :name, :date, :blur_detection_index, :location

  has_one :collection
  has_many :tags
end
