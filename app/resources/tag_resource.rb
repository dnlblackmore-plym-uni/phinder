class TagResource < JSONAPI::Resource
  attributes :name

  has_many :photos
end
