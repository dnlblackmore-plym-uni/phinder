class CollectionResource < JSONAPI::Resource
  attributes :name

  has_many :collections
  has_many :photos
end
