class Photo < ApplicationRecord
  belongs_to :collections
  has_and_belongs_to_many :tags, join_table: 'photo_tag_associations'
end
