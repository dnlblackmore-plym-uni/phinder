class Collection < ApplicationRecord
  has_many :collections
  has_many :photos
end
