FROM docker.io/library/ruby:3.1.2-bullseye

WORKDIR /usr/src/install
COPY Gemfile* ./
RUN apt update -y && \
    apt install -y libmagickwand-dev && \
    rm -rf -- /var/lib/apt/lists/* && \
    awk '{gsub(/<policy domain="resource" name="disk" value=".+"\/>/,"<policy domain=\"resource\" name=\"disk\" value=\"8GiB\"/>")}1' < /etc/ImageMagick-6/policy.xml > /etc/ImageMagick-6/policy.xml.new && \
    mv /etc/ImageMagick-6/policy.xml.new /etc/ImageMagick-6/policy.xml && \
    bundle install
WORKDIR /usr/src/app

CMD ["rails", "server", "-b", "0.0.0.0"]
