# COMP3000 2023-24 Project

## How to Run
To run this code it is recommended that Docker or Podman is used.

If you intend to use Podman read '[Using Podman](#using-podman),' then return here. Only one of Docker or Podman need be installed for these instructions.

If instead, you intend to use Docker then it must be installed, install instructions can be found by searching, "How to install Docker?"

Before running the application, the Docker image must be built. To build the image, run the following command in the root of the repository:

```shell
$ make docker_image
```

With the image built, the application's development environment can be run with the following command:

```shell
$ make
```

For the production environment run the following:

```shell
$ make production
```

### Using Podman
If you intend to use Docker this section is unnecessary.

For this Podman must be installed, install guides can be found by searching, "How to install Podman?"

With Podman installed, simply append `docker=podman` to all the aforementioned commands. This is important if you intend to use Podman instead of Docker.
