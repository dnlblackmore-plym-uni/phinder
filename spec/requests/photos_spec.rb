require 'rails_helper'

# SRC: https://gist.github.com/ascendbruce/7070951
def test_json_string(test_string)
  json_object = JSON.parse(test_string)
  json_object.is_a?(Hash) || json_object.is_a?(Array)
rescue JSON::ParserError, TypeError
  false
end

Capybara.current_driver = :rack_test_json_api

RSpec.describe "Photos", type: :request do
  describe "GET /photos" do
    it "returns JSON" do
      get "http://localhost:3000/photos"
      expect(response).to be_successful
      expect(response.content_type).to eq("application/vnd.api+json")
      assert(test_json_string(response.body))
    end
  end
end
