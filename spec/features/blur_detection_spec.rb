require 'rails_helper'

class FocusTestImage < ActiveRecord::Base
end

RSpec.feature 'BlurDetection', type: :feature do
  set_fixture_class focus_test_images: FocusTestImage
  fixtures :focus_test_images

  it 'ranks photos accordingly' do
    focused_path = focus_test_images(:focused).image_path
    poor_focus_path = focus_test_images(:poor_focus).image_path

    focused_edge_map = BlurDetection.edge_detect(focused_path)
    poor_focus_edge_map = BlurDetection.edge_detect(poor_focus_path)

    focused_focus_rating = BlurDetection.rate_focus(focused_edge_map)
    poor_focus_focus_rating = BlurDetection.rate_focus(poor_focus_edge_map)

    focused_rating = BlurDetection.rate_image(focused_edge_map, focused_focus_rating)
    poor_focus_rating = BlurDetection.rate_image(poor_focus_edge_map, poor_focus_focus_rating)

    assert(focused_rating > poor_focus_rating)
  end
end
